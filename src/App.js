import React, { Component, useState } from 'react';
import './App.css';
import { Button, Card, Input, Icon } from 'antd';
import Salad from './salad.jpg';
import Towel from './towel.jpg';
import Car from './car.jpg';
import Gift from './gift.jpg';

const SignUpForm = () => {
  const [email, setEmail] = useState('');
  const [status, setStatus] = useState('');

  return (
    <>
      <div className='signUpForm'>
        <h2>I am interested</h2>
        <p>When the service is ready, you will be contacted.</p>
        <div className='signUpBar'>
          <Input
            placeholder='email adress'
            value={email}
            onChange={e => setEmail(e.target.value)}
            suffix={
              status === 'success' ? (
                <Icon type='check' />
              ) : status === 'fetching' ? (
                <Icon type='loading' />
              ) : null
            }
          />
          <Button
            type='primary'
            onClick={() => {
              setStatus('fetching');
              fetch(`https://dev.meesvandongen.nl/mailer/?Email=${email}`).then(
                setStatus('success'),
              );
            }}
          >
            Inform me
          </Button>
        </div>
      </div>
    </>
  );
};

class App extends Component {
  render() {
    return (
      <div className='App'>
        <div className='header'>
          <h1 className='headerContent'>Help elderly with transportation</h1>
          <p className='headerSubContent' />
        </div>
        <div className='overWrapper'>
          <SignUpForm />
        </div>
        <div className='contentSection'>
          <h2>What will I do?</h2>
          <p>
            In your free time, you can use our electric stUmobiel car to help
            elderly people go to their destinations. You will assist them
            getting in and out of the car, drive them to their location and
            perhaps share some stories over a cup of coffee.
          </p>
        </div>
        <div className='contentSection'>
          <h2>Why should I do that?</h2>
          <p>
            Right now, many elderly are suffering from social isolation as they
            are not able to move as much as they used to. Going to the store is
            a long voyage and their friends might live on the other side of
            town. Many elderly have meant so much for our society, it's time we
            give something back!
          </p>
        </div>
        <div className='contentSection'>
          <h2>What can I gain?</h2>
          <p>
            We understand that charity work requires a lot of time and effort,
            which is why we want to reward those that help this vulnerable part
            of society. As a token of gratitude, you can save up credits that
            you can spend on various rewards. What about a nice dinner or a
            coupon for traveling?
          </p>
        </div>
        <div className='rewardExamples'>
          <Card
            className='rewardExample'
            title=''
            cover={<img alt='example' src={Salad} />}
          >
            Dinner
          </Card>
          <Card
            className='rewardExample'
            title=''
            cover={<img alt='example' src={Towel} />}
          >
            Household services
          </Card>
          <Card
            className='rewardExample'
            title=''
            cover={<img alt='example' src={Car} />}
          >
            Use Stumobiel's car in your free time
          </Card>
          <Card
            className='rewardExample'
            title=''
            cover={<img alt='example' src={Gift} />}
          >
            Various gift cards
          </Card>
        </div>
        <div className='contentSection'>
          <h2>
            What if I don't have the time to help the elderly, but still want to
            help?
          </h2>
          <p>
            In order to do so, you are always free to donate to our cause. Next
            to that, when our car is not being used, you can rent it! Cheaper
            than other car rental services, you can take the car for a journey
            when no elderly requires transport. All the money you pay will go to
            the maintenance of stUmobiel. Next to that, by driving the car
            around, you are promoting our name!
          </p>
        </div>
        <SignUpForm />
      </div>
    );
  }
}

export default App;
